package com.amirgb.daggermvp;

import android.app.Application;


import com.amirgb.daggermvp.data.component.DaggerNetComponent;
import com.amirgb.daggermvp.data.component.NetComponent;
import com.amirgb.daggermvp.data.module.AppModule;
import com.amirgb.daggermvp.data.module.NetModule;
import com.facebook.stetho.Stetho;

/**
 * Created by amirgb on 29/11/16.
 */

public class MyApplication extends Application {
    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("http://jsonplaceholder.typicode.com/"))
                .build();
    }


    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
