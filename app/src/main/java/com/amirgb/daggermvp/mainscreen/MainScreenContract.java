package com.amirgb.daggermvp.mainscreen;

import com.amirgb.daggermvp.models.Post;

import java.util.List;

/**
 * Created by amirgb on 29/11/16.
 */

public interface MainScreenContract {
    interface View {
        void showPosts(List<Post> posts);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadPost();
    }
}
