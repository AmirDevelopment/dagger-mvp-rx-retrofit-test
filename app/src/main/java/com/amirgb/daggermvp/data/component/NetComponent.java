package com.amirgb.daggermvp.data.component;

import com.amirgb.daggermvp.data.module.AppModule;
import com.amirgb.daggermvp.data.module.NetModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by amirgb on 29/11/16.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    Retrofit retrofit();
}
