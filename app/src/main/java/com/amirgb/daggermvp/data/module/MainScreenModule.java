package com.amirgb.daggermvp.data.module;

import com.amirgb.daggermvp.mainscreen.MainScreenContract;
import com.amirgb.daggermvp.util.CustomScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by amirgb on 29/11/16.
 */
@Module
public class MainScreenModule {

    private final MainScreenContract.View mView;

    public MainScreenModule(MainScreenContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @CustomScope
    MainScreenContract.View providesMainScreenContractView() {
        return mView;
    }
}
