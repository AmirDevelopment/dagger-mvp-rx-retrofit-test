package com.amirgb.daggermvp.data.component;

import com.amirgb.daggermvp.data.module.MainScreenModule;
import com.amirgb.daggermvp.util.CustomScope;
import com.amirgb.daggermvp.mainscreen.MainActivity;

import dagger.Component;

/**
 * Created by amirgb on 29/11/16.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = MainScreenModule.class)
public interface MainScreenComponent {
    void inject(MainActivity activity);
}